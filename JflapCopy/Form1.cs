﻿using JflapCopy.Objetos;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace JflapCopy
{
    public partial class Form1 : Form
    {
        OpenFileDialog open = new OpenFileDialog();
        XmlDocument xDoc = new XmlDocument();
        List<Estado> estados= new List<Estado>();
        List<char> alfabeto = new List<char>();
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            open.Filter = "JFlap Files (*.jff)|*.jff";
            open.InitialDirectory = "C:\\\\\\Users\\\\\\Lourdes Zamora\\\\\\Downloads\\\\\\JFLAP";
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            
            
            if (open.ShowDialog() == DialogResult.OK && open.ToString() != " ")
            {
                //Movemos la ruta del archivo a nuestro Textbox creado para su posterior uso.
                txtFileName.Text = open.FileName;
                xDoc.Load(open.FileName);
                XmlNodeList type = xDoc.GetElementsByTagName("type");
                if (type[0].InnerText != "fa")
                {
                    MessageBox.Show("No ha seleccionado un Automata Finito","Error",MessageBoxButtons.OK,MessageBoxIcon.Error);
                    return;
                }
                CargarAutomata();
                ComprobarAFD();
            }

            
            
        }

        private void ComprobarAFD()
        {
            
        }

        private void CargarAutomata()
        {
            XmlNodeList states = xDoc.GetElementsByTagName("state");

            foreach (XmlElement state in states)
            {
                var estado= new Estado()
                {
                    Id = int.Parse(state.Attributes["id"].Value),
                    Name = state.Attributes["name"].Value
                };

                var childNodes = state.ChildNodes.OfType<XmlNode>();
                var x = childNodes.FirstOrDefault(n => n.Name.Equals("x")).InnerText;
                var y = childNodes.FirstOrDefault(n => n.Name.Equals("y")).InnerText;
                var initial = childNodes.Where(n => n.Name.Equals("initial")).Count()>0;
                var final = childNodes.Where(n => n.Name.Equals("final")).Count() > 0;

                estado.Final = final;
                estado.Initial = initial;
                estado.X = double.Parse(x);
                estado.Y = double.Parse(y);
                estados.Add(estado);

            }

            XmlNodeList transitions = xDoc.GetElementsByTagName("transition");

            foreach (XmlNode transition in transitions)
            {
                var childNodes = transition.ChildNodes.OfType<XmlNode>();
                
                var from = childNodes.FirstOrDefault(n => n.Name.Equals("from")).InnerText;
                var to = childNodes.FirstOrDefault(n => n.Name.Equals("to")).InnerText;
                var read = childNodes.FirstOrDefault(n => n.Name.Equals("read")).InnerText;

                int de = int.Parse(from);
                int a = int.Parse(to);
                estados.Find(e => e.Id == de).Transiciones.Add(new Transicion()
                {
                    Read = read[0],
                    To = estados.Find(e => e.Id == a)
                }) ;
            }

            var alfabetoTransiciones = estados.Select(e => e.Transiciones);
            var alfabeto = new List<char>();
            foreach (var miniAlfabeto in alfabetoTransiciones)
            {
                foreach (var item in miniAlfabeto.Select(t => t.Read).ToList())
                {
                    if(!alfabeto.Contains(item))
                        alfabeto.Add(item);
                }
                
            }

            foreach (var estado in estados)
            {
                txtEstados.Text+= estado.Name + ",";
            }

            foreach (var item in alfabeto)
            {
                txtAlfabeto.Text += item + ","; 
            }

            txtInicial.Text = estados.First(e => e.Initial).Name;

            foreach (var final in estados.Where(e=> e.Final).ToList())
            {
                txtFinales.Text += final.Name + ",";
            }


        }

        private void BtnMinimizar_Click(object sender, EventArgs e)
        {
            //paso 0
            var inalcanzables = new List<Estado>();

            foreach (var estado in estados)
            {
                var estadosSinActual = estados.Where(es => es != estado);
                var transiciones = estadosSinActual.Select(es => es.Transiciones);
                bool encontrado=false;
                foreach (var tr in transiciones)
                {
                    if (tr.Where(t => t.To == estado).Count() > 0)
                    {
                        encontrado = true;
                        break;
                    }
                        
                }

                if (!encontrado)
                    inalcanzables.Add(estado);
            }

            estados = estados.Where(es => !inalcanzables.Contains(es)).ToList();
        }
    }
}
