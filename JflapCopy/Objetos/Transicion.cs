﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JflapCopy.Objetos
{
    class Transicion
    {
        public char Read { get; set; }
        public Estado To { get; set; }
    }
}
