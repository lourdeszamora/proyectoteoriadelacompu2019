﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JflapCopy.Objetos
{
    class Estado
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public double X { get; set; }
        public double Y { get; set; }
        public bool Initial { get; set; }
        public bool Final { get; set; }
        public List<Transicion> Transiciones { get; set; } = new List<Transicion>();
    }
}
